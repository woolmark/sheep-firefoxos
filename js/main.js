﻿var WIDTH = 334;
var HEIGHT = 480;
var FPS = 24;
var SHEEP_MAX = 100;
var SHEEP_IMG_00 = './img/sheep00.png';
var SHEEP_IMG_01 = './img/sheep01.png';
var FENCE = './img/fence.png';

enchant(); 

function getJumpX(y) {
	y -= (HEIGHT - 40);
	return (70 - 3 * y / 4);
}

function clearSheepPosition(pos) {
	pos[0] = 0;
	pos[1] = -1;
	pos[2] = 0;
	pos[3] = 0;
}

function setSheepInitPosition(pos) {
	pos[0] = WIDTH + 20;
	pos[1] = (HEIGHT - 40) + (Math.random()*100 % 30)
	pos[2] = 0;
	pos[3] = getJumpX(pos[1]);
}

function addSheep(sheepPosition) {
	for (i = 1; i < SHEEP_MAX; i++) {
		if (sheepPosition[i][1] == -1) {
			setSheepInitPosition(sheepPosition[i]);
			return;
		}
	}
}

function updateSheepState(game, sheep, sheepNum, sheepPosition, sheepState) {
	for ( i = 0; i < SHEEP_MAX; i++) {
		if(sheepPosition[i][1] >= 0) {
			game.rootScene.addChild(sheep[i]);
			sheep[i].x = sheepPosition[i][0];
			sheep[i].y = sheepPosition[i][1];
			if(sheepState % 2 == 0) {
				sheep[i].image = game.assets[SHEEP_IMG_00]; 
			} else {
				sheep[i].image = game.assets[SHEEP_IMG_01]; 
			}
		} else {
			game.rootScene.removeChild(sheep[i]);
		}
	}
}

window.onload = function() {
    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;
	var game = new Game(WIDTH, HEIGHT);
    game.scale = 2/3;

	game.fps = FPS;
	game.preload(SHEEP_IMG_00);
	game.preload(SHEEP_IMG_01);
	game.preload(FENCE);

 	
	game.onload = function() {
		var sheep = new Array(SHEEP_MAX);
		var sheepNum = 1;
		var sheepPosition  = new Array(SHEEP_MAX);
		var addFlag = 0;
		var fence = new Sprite(52, 78);
		var grassSprite = new Sprite(WIDTH, 70);
		var grassSurface = new Surface(WIDTH, 70);
		var sheepState = 0;
		
		grassSurface.context.beginPath();
		grassSurface.context.fillStyle = 'rgba(100, 250, 100, 1)';
		grassSurface.context.fillRect(0, 0, WIDTH, 70);
		grassSprite.image = grassSurface;
		grassSprite.x = 0;
		grassSprite.y = HEIGHT - 70;
		game.rootScene.addChild(grassSprite);
		
		fence.image = game.assets[FENCE];
 		fence.x = 35;
 		fence.y = HEIGHT - 78;
 		game.rootScene.addChild(fence);
 		

		for ( i  = 0; i < SHEEP_MAX; i ++) {
			sheep[i] = new Sprite(17, 12);
			sheep[i].image = game.assets[SHEEP_IMG_00]; 
			sheepPosition[i] = new Array(4);
			clearSheepPosition(sheepPosition[i]);
		}
		setSheepInitPosition(sheepPosition[0]);

		game.rootScene.backgroundColor  = '#7ecef4';
		var label = new Label();
		game.rootScene.addEventListener(Event.ENTER_FRAME, function() {

			label.moveTo(20, 20);
			label.text = "羊が " + sheepNum + " 匹";
			game.rootScene.removeChild(label);
			game.rootScene.addChild(label);
			if(addFlag == 1) {
				addSheep(sheepPosition);
			}
			for (i = 0; i < SHEEP_MAX; i++) {
				if (sheepPosition[i][1] >= 0) {
					sheepPosition[i][0] -= 5;
					if ((sheepPosition[i][3] - 20) < sheepPosition[i][0]
						&& sheepPosition[i][0] <= sheepPosition[i][3]) {
						sheepPosition[i][1] -= 3;
					} else {
						if ((sheepPosition[i][3] - 40) < sheepPosition[i][0]
							&& sheepPosition[i][0] <= (sheepPosition[i][3] - 20)) {
							sheepPosition[i][1] += 3;
						}
						if (sheepPosition[i][0] < (sheepPosition[i][3] - 10)
							&& sheepPosition[i][2] == 0) {
							sheepNum++;
							sheepPosition[i][2] = 1;
						}
					}
					if (sheepPosition[i][0] < -20) {
						if (i == 0) {
							setSheepInitPosition(sheepPosition[0]);
						} else {
							clearSheepPosition(sheepPosition[i]);
						}
					}
				}
			}
			updateSheepState(game, sheep, sheepNum, sheepPosition, sheepState);
			sheepState++;
		});
		
		game.rootScene.addEventListener(Event.TOUCH_START, function(e) {
			addFlag = 1;
		});
		game.rootScene.addEventListener(Event.TOUCH_END, function(e) {
			addFlag = 0;
		});
    }
    game.start();
};
